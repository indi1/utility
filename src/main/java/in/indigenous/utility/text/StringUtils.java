package in.indigenous.utility.text;

import java.util.ArrayList;
import java.util.List;

public final class StringUtils {
	
	private StringUtils() {
		// No Instantiation
	}
	
	public static String separate(final List<String> list, final String separator) {
		StringBuilder builder = new StringBuilder();
		for(String part: list) {
			builder.append(part);
			if(list.indexOf(part) < list.size() -1) {
				builder.append(separator);
			}
		}
		return builder.toString();
	}
	
	public static List<String> encloseBetween(final List<String> list, final String encloser) {
		List<String> result = new ArrayList<>();
		for(String part: list) {
			StringBuilder builder = new StringBuilder(encloser);
			builder.append(part);
			builder.append(encloser);
			result.add(builder.toString());
		}
		return result;
	}
	
	public static final String append(String src, String ...args) {
		if(org.apache.commons.lang3.StringUtils.isEmpty(src)) {
			return org.apache.commons.lang3.StringUtils.EMPTY;
		}
		StringBuilder builder = new StringBuilder(src);
		if(args != null) {
			for(String arg: args) {
				builder.append(arg);
			}
		}
		return builder.toString();
	}

}
