package in.indigenous.utility.log;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class IndigenousLogger {

	private static Map<Class<?>, IndigenousLogger> class_logger;

	private static Map<String, IndigenousLogger> name_logger;

	private Logger log;

	private IndigenousLogger() {
		// No Instantiation.
	}

	private IndigenousLogger(Logger log) {
		this.log = log;
	}

	static {
		class_logger = new HashMap<>();
		name_logger = new HashMap<>();
	}

	public static IndigenousLogger getInstance(String name) {
		IndigenousLogger log = name_logger.get(name);
		if (log == null) {
			log = new IndigenousLogger(getLogger(name));
			name_logger.put(name, log);
		}
		return log;
	}
	
	public static IndigenousLogger getInstance(Class<?> clazz) {
		IndigenousLogger log = class_logger.get(clazz);
		if (log == null) {
			log = new IndigenousLogger(getLogger(clazz));
			class_logger.put(clazz, log);
		}
		return log;
	}
	
	
	private static Logger getLogger(String name) {
		return LogManager.getLogger(name);
	}
	
	private static Logger getLogger(Class<?> clazz) {
		return LogManager.getLogger(clazz);
	}

	public void info(final String message) {
		if (log.isInfoEnabled()) {
			log.info(message);
		}
	}

	public void debug(final String message) {
		if (log.isDebugEnabled()) {
			log.debug(message);
		}
	}

	public void warn(final String message) {
		if (log.isWarnEnabled()) {
			log.warn(message);
		}
	}

	public void error(final String message, final Throwable t) {
		if (log.isErrorEnabled()) {
			log.error(message, t);
		}
	}

	public void trace(final String message) {
		if (log.isTraceEnabled()) {
			log.trace(message);
		}
	}
	
	public String getName() {
		return log.getName();
	}
	
	public String getLevel() {
		return log.getLevel().name();
	}

}
