package in.indigenous.utility.xml.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import in.indigenous.utility.log.IndigenousLogger;
import in.indigenous.utility.xml.XMLUtil;

public class DefaultXMLUtil implements XMLUtil {
	
	private static final IndigenousLogger LOG = IndigenousLogger.getInstance(DefaultXMLUtil.class); 

	@Override
	public boolean validateXML(final String xsdUrl, final String xmlPath) {
		File file = new File(xmlPath);
		LOG.debug(">>>>>>>>>>>>>>>>>> xml file path :: " + file.getAbsolutePath());
		try {
			SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
			Schema schema = factory.newSchema(new URL(xsdUrl));
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(new File(xmlPath)));
		} catch (IOException | SAXException e) {
			LOG.error("Error parsing xml", e);
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T generate(final Class<T> t, final String xmlPath) {
		try {
			JAXBContext context = JAXBContext.newInstance(t);
			Unmarshaller um = context.createUnmarshaller();
			return (T)um.unmarshal(new FileReader(xmlPath));
		} catch (JAXBException | FileNotFoundException e) {
			LOG.error("Error generating jaxb" , e);
			e.printStackTrace();
		}
		return null;
	}

}
