package in.indigenous.utility.xml;

public interface XMLUtil {
	
	boolean validateXML(final String xsdUrl, final String xmlPath);
	
	<T> T generate(final Class<T> t, final String xmlPath);
	
}
