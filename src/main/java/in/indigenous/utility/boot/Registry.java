package in.indigenous.utility.boot;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public final class Registry {
	
	private Registry() {
		// No Instantiation
	}
	
	public static final ApplicationContext getApplicationContext() {
		System.out.println("getApplicationContext called ...");
		return new ClassPathXmlApplicationContext("classpath:META-INF/config/*-context.xml");
	}

}
