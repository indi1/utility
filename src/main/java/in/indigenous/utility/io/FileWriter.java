package in.indigenous.utility.io;

public interface FileWriter {

	void write(final String dir, final String fileName, final String extn, final String content);
}
