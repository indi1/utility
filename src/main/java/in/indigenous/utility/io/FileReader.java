package in.indigenous.utility.io;

public interface FileReader {
	String readFile(final String filePath);
}
