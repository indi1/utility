package in.indigenous.utility.io.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import in.indigenous.utility.io.FileWriter;
import in.indigenous.utility.log.IndigenousLogger;

public class DefaultFileWriter implements FileWriter {
	
	private static final IndigenousLogger LOG = IndigenousLogger.getInstance("DefaultFileWriter");

	@Override
	public void write(String dir, String fileName, String extn, String content) {
		System.out.println("The log level for [" + LOG.getName() + "] is [" + LOG.getLevel() + "]");
		File dirFile = new File(dir);
		dirFile.mkdirs();
		String fileStr = new StringBuilder(dir).append("/").append(fileName).append(".").append(extn).toString();
		File file = new File(fileStr);
		LOG.debug("Creating file " + file.getAbsolutePath());
		try(PrintWriter writer = new PrintWriter(file)) {
			writer.write(content);
		} catch (FileNotFoundException e) {
			LOG.error("Error creating file [" + file.getAbsolutePath() + "]", e);
			e.printStackTrace();
		}
	}

}
