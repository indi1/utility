package in.indigenous.utility.io.impl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

import in.indigenous.utility.io.FileReader;

public class DefaultFileReader implements FileReader {

	@Override
	public String readFile(String filePath) {
		StringBuilder builder = new StringBuilder();
		try(BufferedReader br = new BufferedReader(new java.io.FileReader(filePath))) {
			String line = null;
			while((line = br.readLine())!= null) {
				builder.append(line);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}

}
