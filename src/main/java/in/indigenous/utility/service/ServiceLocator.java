package in.indigenous.utility.service;

import java.util.Map;

public class ServiceLocator<K, V> {

	private Map<K, V> _map;

	public ServiceLocator() {

	}

	public ServiceLocator(final Map<K, V> map) {
		this._map = map;
	}

	public V locate(K key) {
		return _map.get(key);
	}

	public void setMap(final Map<K, V> map) {
		this._map = map;
	}

}
