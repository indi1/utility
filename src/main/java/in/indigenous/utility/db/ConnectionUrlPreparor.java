package in.indigenous.utility.db;

import in.indigenous.util.db.model.DatabaseParams;

public interface ConnectionUrlPreparor {

	String getConnectionUrl(final DatabaseParams params);
}
