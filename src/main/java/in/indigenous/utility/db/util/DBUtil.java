package in.indigenous.utility.db.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import in.indigenous.util.db.model.Configuration;

public final class DBUtil {

	private DBUtil() {
		// No Instantiation.
	}

	public static Connection getConnection(final Configuration conf) {
		try {
			Class.forName(conf.getDriver());
			return DriverManager.getConnection(conf.getConnectionUrl(), conf.getUser(), conf.getPassword());
		} catch (ClassNotFoundException e) {
			// TODO LOG ERROR.
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO LOG ERROR.
			e.printStackTrace();
		}
		return null;
	}

	public static Connection getTestConnection(final Configuration conf) {
		try {
			Class.forName(conf.getDriver());
			return DriverManager.getConnection(conf.getTestConnectionUrl(), conf.getTestUser(), conf.getTestPassword());
		} catch (ClassNotFoundException e) {
			// TODO LOG ERROR.
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO LOG ERROR.
			e.printStackTrace();
		}
		return null;
	}

}
