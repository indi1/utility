package in.indigenous.utility.db.loader;

import in.indigenous.util.db.model.Configuration;

public interface DatabaseDataLoader {
	
	void load(final Configuration conf);

}
