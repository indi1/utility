package in.indigenous.utility.db.builder.impl;

import java.net.URL;

import org.dbunit.dataset.IDataSet;
import org.dbunit.util.fileloader.CsvDataFileLoader;

import in.indigenous.util.db.model.Configuration;
import in.indigenous.utility.db.builder.DatabaseLoaderBuilder;

public class CsvDatabaseLoaderBuilder implements DatabaseLoaderBuilder {
	
	private CsvDataFileLoader builder;

	@Override
	public IDataSet build(Configuration conf) throws Exception {
		builder = new CsvDataFileLoader();
		URL url = new URL(conf.getDataFile());
		return builder.loadDataSet(url);
	}

}
