package in.indigenous.utility.db.builder.impl;

import java.net.URL;

import org.dbunit.dataset.IDataSet;
import org.dbunit.util.fileloader.FullXmlDataFileLoader;
import org.springframework.util.ResourceUtils;

import in.indigenous.util.db.model.Configuration;
import in.indigenous.utility.db.builder.DatabaseLoaderBuilder;

public class XmlDatabaseLoaderBuilder implements DatabaseLoaderBuilder {

	private FullXmlDataFileLoader builder;

	public IDataSet build(final Configuration conf) throws Exception {
		builder = new FullXmlDataFileLoader();
		URL url = ResourceUtils.getURL(conf.getDataFile());
		System.out.println("URL == " + url);
		return builder.loadDataSet(url);
	}

}
