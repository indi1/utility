package in.indigenous.utility.db.builder;

import org.dbunit.dataset.IDataSet;

import in.indigenous.util.db.model.Configuration;

public interface DatabaseLoaderBuilder {
	
	IDataSet build(final Configuration conf) throws Exception;

}
