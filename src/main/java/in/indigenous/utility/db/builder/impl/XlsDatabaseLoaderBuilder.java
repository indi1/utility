package in.indigenous.utility.db.builder.impl;

import java.net.URL;

import org.dbunit.dataset.IDataSet;
import org.dbunit.util.fileloader.XlsDataFileLoader;

import in.indigenous.util.db.model.Configuration;
import in.indigenous.utility.db.builder.DatabaseLoaderBuilder;

public class XlsDatabaseLoaderBuilder implements DatabaseLoaderBuilder {
	
	private XlsDataFileLoader builder;

	@Override
	public IDataSet build(Configuration conf) throws Exception {
		builder = new XlsDataFileLoader();
		URL url = new URL(conf.getDataFile());
		return builder.loadDataSet(url);
	}

}
