package in.indigenous.utility.db;

import in.indigenous.util.db.model.Configuration;

public interface TestDataPopulator {
	
	void populate(final Configuration conf);
}
