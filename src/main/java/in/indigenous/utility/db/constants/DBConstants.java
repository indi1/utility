package in.indigenous.utility.db.constants;

public final class DBConstants {
	
	private DBConstants() {
		
	}
	
	public final static String MYSQL_CONN_URL_PREFIX = "jdbc:mysql://";
	
	public final static String DB_SCRIPT_PATH = "./src/main/resources/sql/";

}
