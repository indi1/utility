package in.indigenous.utility.db;

import in.indigenous.util.db.model.Configuration;

public interface DatabaseScriptGenerator {

	Configuration generate(final Configuration conf);
	
	Configuration generateTest(final Configuration conf);
	
}
