package in.indigenous.utility.db;

import in.indigenous.util.db.model.Configuration;

public interface TestDatabaseScriptExecutor {

	void execute(final Configuration conf);
}
