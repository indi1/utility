package in.indigenous.utility.db.mysql;

import in.indigenous.utility.db.constants.DBConstants;
import in.indigenous.util.db.model.DatabaseParams;
import in.indigenous.utility.db.ConnectionUrlPreparor;

public class MySQLConnectionUrlPreparor implements ConnectionUrlPreparor {

	private static final String SEPARATOR = "/";

	@Override
	public String getConnectionUrl(final DatabaseParams params) {
		return new StringBuilder(DBConstants.MYSQL_CONN_URL_PREFIX).append(params.getServer()).append(SEPARATOR)
				.append(params.getDbName()).toString();
	}

}
