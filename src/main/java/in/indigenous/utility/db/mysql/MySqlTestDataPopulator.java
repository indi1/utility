package in.indigenous.utility.db.mysql;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.ext.mysql.MySqlMetadataHandler;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.beans.factory.annotation.Autowired;

import in.indigenous.util.db.constants.DataFileFormat;
import in.indigenous.util.db.model.Configuration;
import in.indigenous.utility.db.TestDataPopulator;
import in.indigenous.utility.db.builder.DatabaseLoaderBuilder;
import in.indigenous.utility.db.util.DBUtil;
import in.indigenous.utility.service.ServiceLocator;

public class MySqlTestDataPopulator implements TestDataPopulator {

	@Autowired
	private ServiceLocator<DataFileFormat, DatabaseLoaderBuilder> dataFileLoaderBuilderLocator;

	@Override
	public void populate(Configuration conf) {
		try {
			if (dataFileLoaderBuilderLocator == null) {
				System.err.println("could not load dataFileLoaderBuilderLocator");
			} else if (dataFileLoaderBuilderLocator.locate(conf.getDataFormat()) == null) {
				System.err.println("Could not find dataFileLoaderBuilderLocator for key " + conf.getDataFormat());
			}
			IDataSet dataset = dataFileLoaderBuilderLocator.locate(conf.getDataFormat()).build(conf);
			DatabaseOperation.CLEAN_INSERT.execute(getConnection(conf), dataset);
		} catch (Exception e) {
			// TODO LOG Error.
			e.printStackTrace();
		}
	}

	private IDatabaseConnection getConnection(final Configuration conf) throws DatabaseUnitException {
		DatabaseConnection conn = new DatabaseConnection(DBUtil.getTestConnection(conf), conf.getTestDatabaseName());
		DatabaseConfig config = conn.getConfig();
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
		config.setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());
		config.setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, Boolean.TRUE);
		return conn;
	}

}
