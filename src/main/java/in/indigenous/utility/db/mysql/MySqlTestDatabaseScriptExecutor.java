package in.indigenous.utility.db.mysql;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.ibatis.jdbc.ScriptRunner;

import in.indigenous.util.db.model.Configuration;
import in.indigenous.utility.db.TestDatabaseScriptExecutor;
import in.indigenous.utility.db.constants.DBConstants;
import in.indigenous.utility.text.StringUtils;

public class MySqlTestDatabaseScriptExecutor implements TestDatabaseScriptExecutor {

	@Override
	public void execute(Configuration conf) {
		try {
			Class.forName(conf.getDriver());
		} catch (ClassNotFoundException e) {
			// TODO Log Error
			e.printStackTrace();
		}
		try {
			Connection conn = DriverManager.getConnection(conf.getConnectionUrl(), conf.getUser(), conf.getPassword());
			ScriptRunner runner = new ScriptRunner(conn);
			Reader reader = new BufferedReader(
					new FileReader(StringUtils.append(DBConstants.DB_SCRIPT_PATH, conf.getTestDatabaseName(), ".SQL")));
			runner.runScript(reader);
		} catch (SQLException e) {
			// TODO Log Error
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
