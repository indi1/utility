package in.indigenous.utility.db.mysql;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import in.indigenous.util.db.metadata.DBMetadataPopulator;
import in.indigenous.util.db.metadata.model.ColumnMetadata;
import in.indigenous.util.db.metadata.model.ConstraintMetadata;
import in.indigenous.util.db.metadata.model.DatabaseMetadata;
import in.indigenous.util.db.metadata.model.TableMetadata;
import in.indigenous.util.db.model.Configuration;
import in.indigenous.utility.db.DatabaseScriptGenerator;
import in.indigenous.utility.db.constants.DBConstants;
import in.indigenous.utility.io.FileWriter;
import in.indigenous.utility.log.IndigenousLogger;

public class MySqlDatabaseScriptGenerator implements DatabaseScriptGenerator {

	@Autowired
	private DBMetadataPopulator mysqlDbMetadataPopulator;

	@Autowired
	private FileWriter defaultFileWriter;

	private static final IndigenousLogger LOG = IndigenousLogger.getInstance(MySqlDatabaseScriptGenerator.class);

	private static final String COMMENT = "# SQL SCRIPT FOR database %s.";

	private static final char SPECIAL_CHAR = '`';

	private static final String CREATE_TABLE = "CREATE TABLE ";

	private static final String PRIMARY_KEY = "PRIMARY KEY";

	private static final String ALTER_TABLE = "ALTER TABLE ";

	private static final String ADD_CONSTRAINT = "ADD CONSTRAINT ";

	private static final String REFERENCES = "REFERENCES";

	private static final String AUTO_INCREMENT = "AUTO_INCREMENT";

	private static final String SPACE = " ";

	private static final String FORMAT_SPACE = "   ";

	private static final String PAREN_OPEN = "(";

	private static final String PAREN_CLOSE = ")";

	private static final String NEW_LINE = "\n";

	private static final String COMMA = ",";

	private static final String SEMI_COLON = ";";

	private static final String TABLE_SUFFIX = ") ENGINE=InnoDB DEFAULT CHARSET=latin1;";

	private static final String CREATE_DB = "CREATE DATABASE IF NOT EXISTS ";

	private static final String DROP_DB = "DROP DATABASE IF EXISTS ";

	private static final String DROP_TABLE = "DROP TABLE IF EXISTS ";

	private static final String DROP_USER = "DROP USER IF EXISTS <username>;";

	private static final String CREATE_USER = "CREATE USER <username> IDENTIFIED BY <password>;";

	private static final String GRANT_USER = "GRANT ALL PRIVILEGES ON <db>.* TO <username>;";

	@Override
	public Configuration generate(final Configuration conf) {
		if (StringUtils.isEmpty(conf.getScript())) {
			conf.setScript(getScriptPath(conf.getDatabaseName()));
			conf.setDatabaseMetadata(mysqlDbMetadataPopulator.getDatabaseMetadata(conf));
		} else {
			conf.setDatabaseMetadata(mysqlDbMetadataPopulator.getDatabaseMetadataFromScript(conf));
		}
		String content = getSQLContent(conf, false);
		defaultFileWriter.write(DBConstants.DB_SCRIPT_PATH, conf.getDatabaseName(), "SQL", content);
		return conf;
	}

	@Override
	public Configuration generateTest(Configuration conf) {
		if (StringUtils.isEmpty(conf.getScript())) {
			conf.setTestScript(getScriptPath(conf.getTestDatabaseName()));
			String content = getSQLContent(conf, true);
			defaultFileWriter.write(DBConstants.DB_SCRIPT_PATH, conf.getTestDatabaseName(), "SQL", content);
		}
		return conf;
	}

	private String getScriptPath(final String dbName) {
		return in.indigenous.utility.text.StringUtils.append(DBConstants.DB_SCRIPT_PATH, "/", dbName, ".SQL");
	}

	private String getSQLContent(final Configuration conf, final boolean test) {
		StringBuilder builder = new StringBuilder();
		builder.append(String.format(COMMENT, test ? conf.getTestDatabaseName() : conf.getDatabaseName()));
		builder.append(NEW_LINE);
		builder.append(NEW_LINE);
		if (test) {
			builder.append(getDropDatabaseSQL(conf.getTestDatabaseName()));
			builder.append(NEW_LINE);
			builder.append(getCreateDatabaseSQL(conf.getTestDatabaseName()));
			builder.append(NEW_LINE);
			builder.append(getDropTableSQL(conf.getDatabaseMetadata(), conf.getTestDatabaseName()));
		}
		if (CollectionUtils.isNotEmpty(conf.getDatabaseMetadata().getTables())) {
			for (TableMetadata tableMetadata : conf.getDatabaseMetadata().getTables()) {
				if (test) {
					builder.append(getCreateTableSQL(conf.getTestDatabaseName(), tableMetadata));
				} else {
					builder.append(getCreateTableSQL(conf.getDatabaseName(), tableMetadata));
				}
				builder.append(NEW_LINE);
				for (ConstraintMetadata constraintMetadata : tableMetadata.getConstraints()) {
					builder.append(getConstraintSQL(constraintMetadata));
					builder.append(NEW_LINE);
				}
				builder.append(NEW_LINE);
				builder.append(NEW_LINE);
			}
		}
		if (test) {
			builder.append(getUserScript(conf.getTestUser(), conf.getTestPassword(), conf.getTestDatabaseName()));
		}
		return builder.toString();
	}

	private String getDropTableSQL(final DatabaseMetadata metadata, final String testDbName) {
		StringBuilder builder = new StringBuilder();
		if (CollectionUtils.isNotEmpty(metadata.getTables())) {
			for (TableMetadata tableMetadata : metadata.getTables()) {
				builder.append(in.indigenous.utility.text.StringUtils.append(DROP_TABLE,  ".", String.valueOf(SPECIAL_CHAR),
						testDbName, String.valueOf(SPECIAL_CHAR), ".", String.valueOf(SPECIAL_CHAR),
						tableMetadata.getTableName(), String.valueOf(SPECIAL_CHAR), SEMI_COLON));
				builder.append(NEW_LINE);
				builder.append(NEW_LINE);
			}
		}
		return builder.toString();
	}

	private String getCreateTableSQL(final String dbname, final TableMetadata tableMetadata) {
		StringBuilder builder = new StringBuilder(CREATE_TABLE);
		builder.append(SPECIAL_CHAR);
		builder.append(dbname);
		builder.append(SPECIAL_CHAR);
		builder.append(".");
		builder.append(SPECIAL_CHAR);
		builder.append(tableMetadata.getTableName());
		builder.append(SPECIAL_CHAR);
		builder.append(SPACE);
		builder.append(PAREN_OPEN);
		builder.append(NEW_LINE);
		List<ColumnMetadata> columns = tableMetadata.getColumns();
		for (ColumnMetadata columnMetadata : columns) {
			builder.append(getColumnSQL(columnMetadata));
			if (columns.indexOf(columnMetadata) < columns.size() - 1) {
				builder.append(NEW_LINE);
			}
		}
		String pkSQL = getPrimaryKeySQL(tableMetadata);
		if (StringUtils.isNotEmpty(pkSQL)) {
			builder.append(NEW_LINE);
			builder.append(pkSQL);
		}
		builder.append(NEW_LINE);
		builder.append(TABLE_SUFFIX);
		return builder.toString();
	}

	private String getColumnSQL(final ColumnMetadata columnMetadata) {
		StringBuilder builder = new StringBuilder(FORMAT_SPACE);
		builder.append(SPECIAL_CHAR);
		builder.append(columnMetadata.getColumnName());
		builder.append(SPECIAL_CHAR);
		builder.append(SPACE);
		builder.append(columnMetadata.getDataType());
		if (StringUtils.isNotEmpty(columnMetadata.getSize())) {
			builder.append(PAREN_OPEN);
			builder.append(columnMetadata.getSize());
			builder.append(PAREN_CLOSE);
		}
		builder.append(SPACE);
		if (!columnMetadata.isNullable()) {
			builder.append("NOT NULL");
		}
		if (columnMetadata.isAutoIncrement()) {
			builder.append(SPACE);
			builder.append(AUTO_INCREMENT);
		}
		builder.append(COMMA);
		return builder.toString();
	}

	private String getPrimaryKeySQL(final TableMetadata tableMetadata) {
		String pkList = getPrimaryKeyList(tableMetadata);
		StringBuilder builder = new StringBuilder();
		if (StringUtils.isNotEmpty(pkList)) {
			builder.append(FORMAT_SPACE);
			builder.append(PRIMARY_KEY);
			builder.append(SPACE);
			builder.append(PAREN_OPEN);
			builder.append(pkList);
			builder.append(PAREN_CLOSE);
		}
		return builder.toString();
	}

	private String getPrimaryKeyList(final TableMetadata tableMetadata) {
		LOG.debug(">>>>>>>>>>>>>>>>> PK LIST :: " + tableMetadata.getPrimaryKeys());
		if (CollectionUtils.isNotEmpty(tableMetadata.getPrimaryKeys())) {
			List<String> formatted = in.indigenous.utility.text.StringUtils
					.encloseBetween(tableMetadata.getPrimaryKeys(), String.valueOf(SPECIAL_CHAR));
			return in.indigenous.utility.text.StringUtils.separate(formatted, COMMA);
		}
		return null;
	}

	private String getConstraintSQL(final ConstraintMetadata constraintMetadata) {
		StringBuilder builder = new StringBuilder(ALTER_TABLE);
		builder.append(constraintMetadata.getTableName());
		builder.append(NEW_LINE);
		builder.append(ADD_CONSTRAINT);
		builder.append(constraintMetadata.getConstraintName());
		builder.append(NEW_LINE);
		builder.append(constraintMetadata.getConstraintType());
		builder.append(SPACE);
		builder.append(PAREN_OPEN);
		builder.append(constraintMetadata.getColumnName());
		builder.append(PAREN_CLOSE);
		builder.append(SPACE);
		builder.append(REFERENCES);
		builder.append(SPACE);
		builder.append(constraintMetadata.getReferencedTableName());
		builder.append(PAREN_OPEN);
		builder.append(constraintMetadata.getReferencedColumnName());
		builder.append(PAREN_CLOSE);
		builder.append(SEMI_COLON);
		return builder.toString();
	}

	private String getCreateDatabaseSQL(final String dbName) {
		return in.indigenous.utility.text.StringUtils.append(CREATE_DB, String.valueOf(SPECIAL_CHAR), dbName,
				String.valueOf(SPECIAL_CHAR), SEMI_COLON);
	}

	private String getDropDatabaseSQL(final String dbName) {
		return in.indigenous.utility.text.StringUtils.append(DROP_DB, String.valueOf(SPECIAL_CHAR), dbName,
				String.valueOf(SPECIAL_CHAR), SEMI_COLON);
	}

	private String getUserScript(final String user, final String password, final String db) {
		StringBuilder builder = new StringBuilder();
		builder.append(DROP_USER.replace("<username>", in.indigenous.utility.text.StringUtils.append("'", user, "'")));
		builder.append(NEW_LINE);
		builder.append(CREATE_USER.replace("<username>", in.indigenous.utility.text.StringUtils.append("'", user, "'"))
				.replace("<password>", in.indigenous.utility.text.StringUtils.append("'", password, "'")));
		builder.append(NEW_LINE);
		builder.append(GRANT_USER.replace("<username>", in.indigenous.utility.text.StringUtils.append("'", user, "'"))
				.replace("<db>", db));
		return builder.toString();
	}

}
