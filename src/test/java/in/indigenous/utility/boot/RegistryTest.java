package in.indigenous.utility.boot;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import in.indigenous.util.db.metadata.mysql.MySqlDBMetadataPopulator;

class RegistryTest {

	@Test
	void testGetApplicationContext() {
		MySqlDBMetadataPopulator mysqlDbMetadataPopulator = (MySqlDBMetadataPopulator) Registry.getApplicationContext()
				.getBean("mysqlDbMetadataPopulator");
		assertNotNull(mysqlDbMetadataPopulator);
	}

}
